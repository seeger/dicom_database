package com.dicom_database.dicom_database.postgress.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dicom_database.dicom_database.postgress.entity.Image;

/**
 * Class for mapping rows back to image object
 */
public class ImageRowMapper implements RowMapper<Image>{
    /**
     * Maps sql-table row back to an image
     * @param rs
     * @param arg1
     * @return
     * @throws SQLException
     */

    @Override
    public Image mapRow(ResultSet rs,int arg1) throws SQLException{
        Image img = new Image();
        img.setInstanceNumber(rs.getInt("InstanceNumber"));
        img.setSeriesDescription(rs.getString("SeriesDescription"));
        img.setStudyID(rs.getInt("StudyID"));
        img.setSeriesNumber(rs.getInt("SeriesNumber"));
        img.setImagepath(rs.getString("Imagepath"));

        img.setRelativepath(rs.getString("relativepath"));
        return img;
    }
}
