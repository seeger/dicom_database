package com.dicom_database.dicom_database.postgress.dicom_reader;

import com.dicom_database.dicom_database.postgress.entity.Image;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.function.Consumer;

import com.pixelmed.dicom.AttributeList;

public interface Dicom_reader {

    AttributeList list = new AttributeList();

    /**
     * Creates image objects for all dicom file in given directory
     * @param dir
     * @return list of image objects
     * @throws IOException
     */
    List<Image> get_all_images(String dir) throws IOException;

    /**
     * Converts an dicom file to an image
     * @param path
     * @return image object
     */
    Image dicom_to_image(String path);



}
