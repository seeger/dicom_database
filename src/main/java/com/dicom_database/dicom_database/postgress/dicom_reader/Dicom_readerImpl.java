package com.dicom_database.dicom_database.postgress.dicom_reader;


import com.dicom_database.dicom_database.postgress.entity.Image;

import java.util.List;
import java.util.ArrayList;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import com.pixelmed.dicom.TagFromName;
import com.pixelmed.dicom.AttributeTag;
import org.springframework.stereotype.Component;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.function.Consumer;
import java.io.IOException;

/**
 * Implements the Dicom_reader interface
 */
@Component
public class Dicom_readerImpl implements Dicom_reader {


    @Override
    public List<Image> get_all_images(String dir) throws IOException {
        List<Image> images = new ArrayList<>();
        Path path = Paths.get(dir);
        File directory = new File(dir);
        this.fetchFiles(directory, images);
        return images;
    }




    private void fetchFiles(File dir, List<Image> images) {
        if (dir.isDirectory()) {
            for (File file1 : dir.listFiles()) {
                fetchFiles(file1, images);
            }
        } else {
            if(dir.getName() != "DICOMDIR" || dir.getName() != "README"){
                Image image =this.dicom_to_image(dir.getAbsolutePath());
                if (image.getSeriesDescription() != "0"){
                        images.add(this.dicom_to_image(dir.getAbsolutePath()));
                }

            }
        }
    }

    @Override
    public Image dicom_to_image(String path){
        Image img = new Image();
        try{
            list.read(path);
            img.setStudyID(Integer.parseInt(this.tag_to_string(TagFromName.StudyID)));
            img.setImagepath(path);
            img.setSeriesNumber(Integer.parseInt(this.tag_to_string(TagFromName.SeriesNumber)));
            img.setSeriesDescription(this.tag_to_string(TagFromName.SeriesDescription));
            img.setStudyDescription(this.tag_to_string(TagFromName.StudyDescription));
            img.setInstanceNumber(Integer.parseInt(this.tag_to_string(TagFromName.InstanceNumber)));
            img.setRelativepath(path.substring(path.indexOf("Daten/")));


        } catch (Exception e) {
            e.printStackTrace();

        }


        return img;
    }
    private String tag_to_string(AttributeTag tag){
        try {
            return list.get(tag).getStringValues()[0].replaceAll("\\s+", "");
        }catch (Exception e){
            e.printStackTrace();
            return "0";
        }
    }


}
