package com.dicom_database.dicom_database.postgress.controller;
import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dicom_database.dicom_database.postgress.entity.Image;
import java.util.ArrayList;
import com.dicom_database.dicom_database.postgress.dicom_reader.Dicom_reader;
import com.dicom_database.dicom_database.postgress.service.ImageService;

/**
 * Contains functions to connect the api with the database
 */
@RestController
@RequestMapping("/dicomdatabaseAPP")
public class ApplicationController {

    @Resource
    ImageService imgservice;

    @Resource
    Dicom_reader dicom_reader;

    /**
     * Finds all images in database
     * @return list of images
     */
    @GetMapping(value = "/ImageList")
    public List<Image> getImages() {

        System.out.println("Finds all images in database");
        return imgservice.findAll();

    }

    /**
     * Deletes all entries in the image table
     */
    @GetMapping(value = "/delete_image_table_entries")
    public void delete_image_table_entries(){

        System.out.println("Deleting all entries of the image table");
        imgservice.deleteImagetable();
    }


    /**
     * Saves all pathes to dicom files in directory to the image table
     * @param path path to directory
     * @throws IOException
     */
    @PostMapping(value="/dir_to_database")
    public void dir_to_database(@RequestBody String path) throws IOException {

        System.out.println("Save all files of directory to database");
        List<Image> images =dicom_reader.get_all_images(path);
        int index =0;
        for(Image img : images){
            imgservice.insertImage(img);

        }
    }




}
