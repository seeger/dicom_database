package com.dicom_database.dicom_database.postgress.entity;
import lombok.Getter;
import lombok.Setter;

/**
 * Class for the image object which is later converted to an entry for the database
 */
public class Image {

    @Getter @Setter int StudyID;
    @Getter @Setter int SeriesNumber;
    @Getter @Setter int InstanceNumber;
    @Getter @Setter String SeriesDescription;
    @Getter @Setter String StudyDescription;
    @Getter @Setter String Imagepath;
    @Getter @Setter String Relativepath;
}

