package com.dicom_database.dicom_database.postgress.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.jdbc.core.JdbcTemplate;
import com.dicom_database.dicom_database.postgress.entity.Image;
import com.dicom_database.dicom_database.postgress.mapper.ImageRowMapper;

/**
 * Implements the ImageDao interface
 */

@Component
public class ImageDaoImpl implements ImageDao{

    JdbcTemplate basic_template;
    NamedParameterJdbcTemplate template;
    public ImageDaoImpl(NamedParameterJdbcTemplate template,JdbcTemplate basic_template){

        this.template = template;
        this.basic_template = basic_template;
    }

    @Override
    public List<Image> findAll(){
        return template.query(("select * from image"),new ImageRowMapper());
    }

    @Override
    public void insertImage(Image img){
        final String sql =
                "INSERT INTO " +
                        "image(studyid,seriesnumber,instancenumber,seriesdescription,studydescription,imagepath,relativepath) " +
                        "VALUES(:studyid,:seriesnumber,:instancenumber,:seriesdescription,:studydescription,:imagepath,:relativepath)"+
                        "ON CONFLICT (relativepath) DO UPDATE SET " +
                        "studyid = EXCLUDED.studyid, seriesnumber = EXCLUDED.seriesnumber, instancenumber= EXCLUDED.instancenumber, seriesdescription =EXCLUDED.seriesdescription,studydescription = EXCLUDED.studydescription,imagepath=EXCLUDED.imagepath";
        KeyHolder holder = new GeneratedKeyHolder();
        SqlParameterSource params= new MapSqlParameterSource()
                .addValue("studyid",img.getStudyID())
                .addValue("seriesnumber",img.getSeriesNumber())
                .addValue("instancenumber",img.getInstanceNumber())
                .addValue("seriesdescription",img.getSeriesDescription())
                .addValue("studydescription",img.getStudyDescription())
                .addValue("imagepath",img.getImagepath())
                .addValue("relativepath",img.getRelativepath());
        template.update(sql,params,holder);
    }



    @Override
    public void deleteImageTable() {
        final String sql =
                "DELETE FROM image";
        basic_template.execute(sql);

    }


}
