package com.dicom_database.dicom_database.postgress.dao;
import java.util.List;
import com.dicom_database.dicom_database.postgress.entity.Image;

public interface ImageDao {

    /**
     * finds all images in image table
     * @return list of images
     */
    List<Image> findAll();

    /**
     * inserts an entry in the Image table
     * @param img image to inesert
     */

    void insertImage(Image img);

    /**
     * deletes all entries in table
     */
    void deleteImageTable();


}
