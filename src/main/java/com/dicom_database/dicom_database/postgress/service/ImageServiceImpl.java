package com.dicom_database.dicom_database.postgress.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.dicom_database.dicom_database.postgress.entity.Image;
import com.dicom_database.dicom_database.postgress.dao.ImageDao;

/**
 * Implements Imageservice
 */
@Component
public class ImageServiceImpl implements ImageService{

    @Resource
    ImageDao imgdao;

    @Override
    public List<Image> findAll(){

        return imgdao.findAll();
    }

    @Override
    public void insertImage(Image img){
        imgdao.insertImage(img);
    }

    @Override
    public void deleteImagetable() {
        imgdao.deleteImageTable();
    }
}
