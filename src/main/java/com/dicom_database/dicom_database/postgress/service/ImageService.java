package com.dicom_database.dicom_database.postgress.service;

import java.util.List;
import com.dicom_database.dicom_database.postgress.entity.Image;


public interface ImageService {
    /**
     * finds all images in table
     * @return
     */
    List<Image> findAll();

    /**
     * Inserts image into table
     * @param img
     */
    void insertImage(Image img);

    /**
     * delets all entries in image table
     */
    void deleteImagetable();




}

