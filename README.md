# Creates dicom paths database for directory
Clone repository with intellij and build and run DicomDatabaseApplication.
Use postman or another api to send requests to the database.


Possible Requests:


    Mapping: /delete_image_table_entries -> deletes all entires in database


    PostMapping /dir_to_database ->insert directory path as string in request body to save all dicom files in database

    
Database has one table image with relations: StudyID, SeriesNumber,InstanceNumber,SeriesDescription, StudyDescription,Imagepath,Relativepath
